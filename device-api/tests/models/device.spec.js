const {
    sequelize,
    dataTypes,
    checkModelName,
    checkPropertyExists
} = require('sequelize-test-helpers')

const DeviceModel = require('../../models/device.js')

describe('Device model', () => {
    const Device = DeviceModel(sequelize, dataTypes)
    const device = new Device()

    checkModelName(Device)('Device')

    checkPropertyExists(device)('name')
    checkPropertyExists(device)('owner')
    checkPropertyExists(device)('productType')
})