const expect = require('chai').expect
const sinon = require('sinon')
const device = require('./devices.js')

describe('Device Routes', () => {
    describe('Create Device POST /devices', () => {
        // Can only create positive test cause code does not validate req parameters and db still accept empty values
        it('Should create new device', async () => {
            let req = {
                body: {
                    'name': 'new device 1',
                    'owner': 'john doe',
                    'productType': 'product'
                }
            }

            let res = {
                send: sinon.spy()
            }

            await device.createDevice(req, res)
           
            expect(res.send.calledOnce).to.be.true
            expect(res.send.firstCall.args[0]).to.deep.equal({
                "id": 1,
                "name": "new device 1",
                "owner": "john doe",
                "productType": "product"
            })
        })
    })

    describe('Get Device using ID GET /devices/:id', () => {
        it('should get device with id 1', async () => {
            let req = {
                body: {
                    'id': 1
                }
            }

            let res = {
                send: sinon.spy()
            }

            await device.getDeviceById(req, res, {
                "id": 1,
                "name": "new device 1",
                "owner": "john doe",
                "productType": "product"
            })
           
            expect(res.send.calledOnce).to.be.true

            expect(res.send.firstCall.args[0]).to.deep.equal({
                "id": 1,
                "name": "new device 1",
                "owner": "john doe",
                "productType": "product"
            })    
        }) 

        it('should not get device with id 2', async () => {
            let req = {
                body: {
                    'id': 1
                }
            }

            let res = {
                send: sinon.spy()
            }
            
            await device.getDeviceById(req, res, {})
           
            expect(res.send.calledOnce).to.be.true

            expect(res.send.firstCall.args[0]).to.deep.equal({
                "id": 2,
                "name": undefined,
                "owner": undefined,
                "productType": undefined
            })    
        }) 
    })

    describe('Get All Devices GET /devices/', () => {
        it('should get all devices', async () => {
            let res = {
                send: sinon.spy()
            }

            await device.getDeviceAllDevices(res)
           
            expect(res.send.calledOnce).to.be.true
            res.send.firstCall.args[0].forEach( (device) => {
                expect(device).to.deep.equal({
                    "id": 1,
                    "name": "new device 1",
                    "owner": "john doe",
                    "productType": "product"
                })    
            })
        }) 
    })

    describe('Update Device PUT /devices/', () => {
        it('should update name device 1 to device 2', async () => {
            let req = {
                body:  {
                    "id": 1,
                    "newValue": {
                        "name": "device 2"
                    }
                }
            }
            let res = {
                send: sinon.spy()
            }

            await device.updateDevice(req, res)
           
            expect(res.send.calledOnce).to.be.true
            res.send.firstCall.args[0].forEach( (device) => {
                expect(device).to.deep.equal({
                    "id": 1,
                    "name": "new device 1",
                    "owner": "john doe",
                    "productType": "product"
                })    
            })
        }) 
    })
})
