const SequelizeMock = require('sequelize-mock');

const createDevice = async (req, res) => {
    const dbMock = new SequelizeMock();
    const DeviceMock = dbMock.define( 'Device', {},  {
        autoQueryFallback: false,
        instanceMethods: {
            getDevice: function () {
                return  {
                    "id": this.get('id'),
                    "name": this.get('name'),
                    "owner": this.get('owner'),
                    "productType": this.get('productType')
                }
            }
        }
    })
    const device = await DeviceMock.create(req.body)

    res.send({
        "id": device.get('id'),
        "name": device.get('name'),
        "owner": device.get('owner'),
        "productType": device.get('productType')
    })
}

const getDeviceById = async (req, res, mockData) => {
    const dbMock = new SequelizeMock();
    const DeviceMock = dbMock.define( 'Device', {},  {
        autoQueryFallback: false,
        instanceMethods: {
            getDevice: function () {
                return  {
                    "id": this.get('id'),
                    "name": this.get('name'),
                    "owner": this.get('owner'),
                    "productType": this.get('productType')
                }
            }
        }
    })

    DeviceMock.$queueResult(DeviceMock.build(mockData));
    const device = await DeviceMock.findById(req.body.id)
    
    res.send({
        "id": device.get('id'),
        "name": device.get('name'),
        "owner": device.get('owner'),
        "productType": device.get('productType')
    })
}

const getDeviceAllDevices = async (res) => {
    const dbMock = new SequelizeMock();
    const DeviceMock = dbMock.define( 'Device', {
        "id": 1,
        "name": "new device 1",
        "owner": "john doe",
        "productType": "product"
    },  {
        instanceMethods: {
            getDevice: function () {
                return  {
                    "id": this.get('id'),
                    "name": this.get('name'),
                    "owner": this.get('owner'),
                    "productType": this.get('productType')
                }
            }
        }
    })
    const devices = await DeviceMock.findAll()
    const response = []
    devices.forEach((device) => {
        response.push({
            "id": device.get('id'),
            "name": device.get('name'),
            "owner": device.get('owner'),
            "productType": device.get('productType')
        })
    })
    res.send(response)
}

const updateDevice = async (req, res) => {
    const dbMock = new SequelizeMock();
    const DeviceMock = dbMock.define( 'Device', {
        "id": 1,
        "name": "device 1",
        "owner": "john doe",
        "productType": "product"
    },  {
        instanceMethods: {
            getDevice: function () {
                return  {
                    "id": this.get('id'),
                    "name": this.get('name'),
                    "owner": this.get('owner'),
                    "productType": this.get('productType')
                }
            }
        }
    })
    const devices = await DeviceMock.update({
        "id": 2,
        "name": "device 2",
        "owner": "john doe",
        "productType": "product"
    })
    console.log(devices)
    // res.send(response)
}


module.exports = {
    createDevice,
    getDeviceById,
    getDeviceAllDevices,
    updateDevice
}